angular.module('dresser.controllers', [])

.controller('DresserCtrl', function ($scope, $state) {
	$scope.Login = function () {
		$state.go('dresser.login');
	}
})
.controller('ReservationsCtrl', function ($scope, $state) {
})

.controller('ReservationsDetailCtrl', function ($scope, $stateParams, $state, Dressers) {
	$scope.dresser = Dressers.get($stateParams.dresserId);
})

.controller('LoginCtrl', function ($scope) { })
.controller('DresserrsListCtrl', function ($scope, Dressers, $state) {
	$scope.dressers = Dressers.all();
	$scope.remove = function (dresser) {
		Dressers.remove(dresser);
	}
	$scope.CreateReservation = function () {
		$state.go('dresser.search');
	}
})

.controller('DressersDetailCtrl', function ($scope, $stateParams, Dressers) {
	$scope.dresser = Dressers.get($stateParams.dresserId);
})

.controller('SearchCtrl', function ($scope, $cordovaDatePicker, $ionicLoading, $cordovaGeolocation) {
	$scope.positions={};
	$scope.centerOnMe = function () {


		$ionicLoading.show({
			template: 'Loading...'
		});

		var posOptions = { timeout: 10000, enableHighAccuracy: false };
		$cordovaGeolocation.getCurrentPosition(posOptions)
	      .then(function (position) {
	      		var lat = position.coords.latitude;
	      		var long = position.coords.longitude;
	      		var geocoder = new google.maps.Geocoder();
	      		var latlng = new google.maps.LatLng(lat,long);
	      		geocoder.geocode({
	      			'latLng': latlng
	      		}, function (results, status) {
	      			if (status === google.maps.GeocoderStatus.OK) {
	      				$scope.positions = results[0];
	      				alert($scope.positions.formatted_address);
	      			} else {
	      				alert('Geocoder failed due to: ' + status);
	      			}
	      		});
	      		$ionicLoading.hide();
	      	});
	}
	var options = {
		date: new Date(),
		mode: 'date', // or 'time'
		minDate: new Date() - 10000,
		allowOldDates: true,
		allowFutureDates: false,
		doneButtonLabel: 'DONE',
		doneButtonColor: '#F2F3F4',
		cancelButtonLabel: 'CANCEL',
		cancelButtonColor: '#000000'
	};

	document.addEventListener("deviceready", function () {
		$scope.SelectDate = function () {
			$cordovaDatePicker.show(options).then(function (date) {
				alert(date);
			});
		}
	}, false);
});
