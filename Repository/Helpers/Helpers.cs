﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Helpers
{
	public static class HelperMethods
	{
		public static string GeneratePairCodeNum(int size = 8)
		{
			char[] chars = null;
			string a;
			a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
			chars = a.ToCharArray();
			byte[] data = new byte[1];
			RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
			crypto.GetNonZeroBytes(data);

			data = new byte[size];
			crypto.GetNonZeroBytes(data);
			StringBuilder result = new StringBuilder(size);
			foreach (byte b in data)
			{ result.Append(chars[b % (chars.Length - 1)]); }
			return result.ToString();
		}
	}
}
