﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Constants
{
	public static class Constants
	{
		public static readonly string InvalidEmailAdress = "Geçersiz mail adresi.";
		public static readonly int InvalidEmailAdressErrorCode = 610;

		public static readonly string EmailInUse = "Email adresi kullanımda.";
		public static readonly int EmailInUseErrorCode = 611;

		public static readonly string ReservationAlreadyCreate = "Rezervasyon önceden oluşturulmuş.";
		public static readonly int ReservationAlreadyCreateErrorCode = 612;

		public static readonly string UserNotFound = "Kullanıcı kaydınız bulunamadı.";
		public static readonly int UserNotFoundErrorCode = 613;

		public static readonly string EntityNotFound = "Kuaför bulunamadı";
		public static readonly int EntityNotFoundErrorCode = 614;

		public static readonly string ReservationNotFound = "Kuaför bulunamadı";
		public static readonly int ReservationNotFoundErrorCode = 615;

		public static readonly string ModelNotFound = "Geçersiz Model";
		public static readonly int ModelNotFoundErrorCode = 616;

		public static readonly string DBError = "Sunucuda beklenmeyen bir hata oluştu. İşleminiz tamamlanamadı.";
		public static readonly int DBErrorCode = 600;

		public static readonly string ModalNotValid = "Eksik parametreler var.";
		public static readonly int ModalNotValidErrorCode = 616;
	}
}
