﻿using Repository.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interface
{
	public interface IRepository
	{
		#region Api-User
		Task<bool> CheckUserEmail(string email);
        Task<ApiReturnType> CreateUserAccount(UserAccountDto p);

		Task<ApiReturnType> UpdateUserProfile(UserAccountDto p);
		#endregion

		#region Api-Entity
		Task<ApiReturnType> GetEntitiy(GetEntityDto u);
		#endregion

		#region Api-Reservation
		Task<ApiReturnType> CreateReservation(ReservationDto r);
		Task<ApiReturnType> GetReservationDetail(string Id);
		Task<ApiReturnType> DeleteReservation(ReservationDto r);
		Task<ApiReturnType> UpdateReservation(ReservationDto r);
		#endregion
	}
}
