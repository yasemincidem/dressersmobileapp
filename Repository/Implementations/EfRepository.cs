﻿using Repository.Dto;
using Repository.Interface;
using Repository.Models;
using Repository.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Repository.Implementations
{
	public class EfRepository : IRepository
	{
		private DressersEntities db = new DressersEntities();
		#region Api-User
		public async Task<bool> CheckUserEmail(string email)
		{
			try
			{

				return await db.Users.Where(u => u.Email == email).CountAsync() > 0;
			}
			catch (Exception e)
			{
				return false;
			}
		}
		public async Task<ApiReturnType> CreateUserLogin(string Email,string HashPassword)
		{
			var returnData = new ApiReturnType();
		    if(await CheckUserEmail(Email))
			{
				var user = await db.Users.Where(r => r.Email == Email && r.Password == HashPassword).FirstOrDefaultAsync();
		    }
			else
			{
				returnData.Success = false;
				returnData.ErrorCode = Constants.Constants.UserNotFoundErrorCode;
				returnData.ErrorMessage = Constants.Constants.UserNotFound;
			}
			try
			{
				await db.SaveChangesAsync();
				returnData.Success = true;
				return returnData;
			}
			catch (Exception)
			{
				returnData.Success = false;
				returnData.ErrorCode = Constants.Constants.DBErrorCode;
				returnData.ErrorMessage = Constants.Constants.DBError;
				return returnData;
			}
			
		}
		public async Task<ApiReturnType> CreateUserAccount(UserAccountDto p)
		{
			var returnData = new ApiReturnType();
			var user = await db.Users.FindAsync(p.Id);
			if (await CheckUserEmail(p.Email))
			{
				returnData.Success = false;
				returnData.ErrorMessage = Constants.Constants.EmailInUse;
				returnData.ErrorCode = Constants.Constants.EmailInUseErrorCode;
			}
			else if (user == null)
			{
				returnData.Success = false;
				returnData.ErrorMessage = Constants.Constants.UserNotFound;
				returnData.ErrorCode = Constants.Constants.UserNotFoundErrorCode;
				return returnData;
			}
			user = new User()
			{
				Email = p.Email,
				FirstName = p.FirstName,
				LastName = p.LastName,
				Password = p.Password,
				PhoneNumber = p.PhoneNumber
			};
			db.Entry(user).State = EntityState.Added;
			db.Users.Add(user);
			try
			{
				await db.SaveChangesAsync();
				returnData.Success = true;
				return returnData;
			}
			catch (Exception)
			{
				returnData.Success = false;
				returnData.ErrorCode = Constants.Constants.DBErrorCode;
				returnData.ErrorMessage = Constants.Constants.DBError;
				return returnData;
			}
		}
		public async Task<ApiReturnType> UpdateUserProfile(UserAccountDto p)
		{
			var returnData = new ApiReturnType();
			var user = await db.Users.FindAsync(p.Id);
			if (user == null)
			{
				returnData.Success = false;
				returnData.ErrorCode = Constants.Constants.UserNotFoundErrorCode;
				returnData.ErrorMessage = Constants.Constants.UserNotFound;
				return returnData;
			}
			user.FirstName = p.FirstName;
			user.LastName = p.LastName;
			user.Email = p.Email;
			user.Password = p.Password;
			user.PhoneNumber = p.PhoneNumber;
			db.Entry(user).State = EntityState.Modified;
			try
			{
				returnData.Success = true;
				await db.SaveChangesAsync();
				return returnData;
			}
			catch (Exception)
			{
				returnData.Success = false;
				returnData.ErrorCode = Constants.Constants.DBErrorCode;
				returnData.ErrorMessage = Constants.Constants.DBError;
				return returnData;
			}

		}
		#endregion
		#region Api-Entity
		public async Task<ApiReturnType> GetEntitiy(GetEntityDto u)
		{
			var returnData = new ApiReturnType();
			var entity = await db.Entities.Where(c => c.Key == u.Key).ToListAsync();
			if (entity == null)
			{
				returnData.Success = false;
				returnData.ErrorCode = Constants.Constants.EntityNotFoundErrorCode;
				returnData.ErrorMessage = Constants.Constants.EntityNotFound;
				return returnData;
			}
			try
			{
				await db.SaveChangesAsync();
				returnData.Success = true;
				return returnData;
			}
			catch (Exception)
			{
				returnData.Success = false;
				returnData.ErrorCode = Constants.Constants.DBErrorCode;
				returnData.ErrorMessage = Constants.Constants.DBError;
				return returnData;
			}

        }
		#endregion
		#region Api-Reservation
		public async Task<ApiReturnType> CreateReservation(ReservationDto r)
		{
			var returnData = new ApiReturnType();
			var reservation = await db.Reservations.FindAsync(r.Id);
			if (reservation != null)
			{
				returnData.Success = false;
				returnData.ErrorCode = Constants.Constants.ReservationAlreadyCreateErrorCode;
				returnData.ErrorMessage = Constants.Constants.ReservationAlreadyCreate;
				return returnData;
			}
			reservation = new Reservation()
			{
				ReservationId = r.Id,
				CheckinDate = r.CheckinDate,
				CheckoutDate = r.CheckOutDate,
				CreateDate = r.CreateDate,
				UpdateDate = r.UpdateDate,
				NumberOfUnits = r.NumberOfUnits,
				UserId=r.UserId
			};
			db.Entry(reservation).State= EntityState.Added;
			db.Reservations.Add(reservation);
			try
			{
				await db.SaveChangesAsync();
				returnData.Success = false;
				return returnData;
			}
			catch (Exception)
			{
				returnData.Success = false;
				returnData.ErrorCode = Constants.Constants.DBErrorCode;
				returnData.ErrorMessage = Constants.Constants.DBError;
				return returnData;
			}

		}
		public async Task<ApiReturnType> GetReservationDetail(string Id)
		{
			var returnData = new ApiReturnType();
			var reservation = await db.Reservations.Where(u => u.ReservationId == Id).ToListAsync();
			if (reservation == null)
			{
				returnData.Success = false;
				returnData.ErrorCode = Constants.Constants.ReservationNotFoundErrorCode;
				returnData.ErrorMessage = Constants.Constants.ReservationNotFound;
				return returnData;
			}
			try
			{
				await db.SaveChangesAsync();
				returnData.Success = true;
				return returnData;
			}
			catch (Exception)
			{
				returnData.Success = false;
				returnData.ErrorCode = Constants.Constants.DBErrorCode;
				returnData.ErrorMessage = Constants.Constants.DBError;
				return returnData;
			}

		}
		public async Task<ApiReturnType> DeleteReservation(ReservationDto r)
		{
			var returnData = new ApiReturnType();
			var reservation = await db.Reservations.FindAsync(r.Id);
			if (reservation == null)
			{
				returnData.Success = false;
				returnData.ErrorCode = Constants.Constants.ReservationNotFoundErrorCode;
				returnData.ErrorMessage = Constants.Constants.ReservationNotFound;
				return returnData;
			}
			db.Reservations.Remove(reservation);
			try
			{
				await db.SaveChangesAsync();
				returnData.Success = true;
				return returnData;
			}
			catch (Exception)
			{
				returnData.Success = false;
				returnData.ErrorCode = Constants.Constants.DBErrorCode;
				returnData.ErrorMessage = Constants.Constants.DBError;
				return returnData;
			}
			
		}
		public async Task<ApiReturnType> UpdateReservation(ReservationDto r)
		{
			var returnData = new ApiReturnType();
			var reservation = await db.Reservations.FindAsync(r.Id);
			if (reservation == null)
			{
				returnData.Success = false;
				returnData.ErrorCode = Constants.Constants.ReservationNotFoundErrorCode;
				returnData.ErrorMessage = Constants.Constants.ReservationNotFound;
				return returnData;
			}
			reservation = new Reservation
			{
				CheckinDate = r.CheckinDate,
				CheckoutDate = r.CheckOutDate,
				CreateDate = r.CreateDate,
				UpdateDate = r.UpdateDate,
				NumberOfUnits = r.NumberOfUnits
			};
			db.Entry(reservation).State = EntityState.Modified;
			try
			{
				await db.SaveChangesAsync();
				returnData.Success = true;
				return returnData;
			}
			catch (Exception)
			{
				returnData.Success = false;
				returnData.ErrorCode = Constants.Constants.DBErrorCode;
				returnData.ErrorMessage = Constants.Constants.DBError;
				return returnData;
			}

		}
		#endregion
	}
}


