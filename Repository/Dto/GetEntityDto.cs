﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Dto
{
	public class GetEntityDto
	{
		[Required]
		public string Key { get; set; }
		[Required, MinLength(2), MaxLength(50)]
		public string DresserName { get; set; }
		[Required, RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$")]
		public string PhoneNumber { get; set; }
		public DateTime StartTime { get; set; }
		public DateTime EndTime { get; set; }
		[Required, MinLength(2), MaxLength(150)]
		public string Address { get; set; }
		[Required, MinLength(2), MaxLength(50)]
		public string OwnerName { get; set; }
	}
}
