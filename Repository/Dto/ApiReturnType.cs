﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Dto
{
	public class ApiReturnType
	{
		public object Data { get; set; }
		public bool Success { get; set; }
		public object ErrorMessage { get; set; }
		public int ErrorCode { get; set; }
	}
}
