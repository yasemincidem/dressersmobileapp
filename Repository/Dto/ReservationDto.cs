﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Dto
{
	public class ReservationDto
	{
		[Required]
		public string Id { get; set; }
		public DateTime CheckinDate { get; set; }
		public DateTime CheckOutDate { get; set; }
		public int NumberOfUnits { get; set; }
		public DateTime CreateDate { get; set; }
		public DateTime UpdateDate { get; set; }
		[Required]
		public string UserId { get; set; }
	}
}
