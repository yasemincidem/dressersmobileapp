﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Dto
{
	public class UserAccountDto
	{
		[Required]
		public Guid Id { get; set; }
		[Required, MinLength(2), MaxLength(50)]
		public string FirstName { get; set; }
		[MinLength(2), MaxLength(50)]
		public string LastName { get; set; }
		[Required,EmailAddress]
		public string Email { get; set; }
		[Required]
		public string Password { get; set; }
		[Required, RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$")]
		public string PhoneNumber { get; set; }
	}
}
