﻿using Repository.Constants;
using Repository.Implementations;
using Repository.Interface;
using Repository.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace DresserWebApi.Controllers
{

	public class UserController : ApiController
    {
		EfRepository repo = new EfRepository();
		[HttpPost]
		[Route("User/CheckUserEmail")]
		public async Task<bool> CheckUserEmail(string email)
		{
			if (string.IsNullOrWhiteSpace(email))//Is empty checks the entered values
			{
				return false;
			}
			return await repo.CheckUserEmail(email); 
		}
		[HttpPost]
		[Route("User/CreateUserLogin")]
		public async Task<object> CreateUserLogin([FromBody]UserAccountDto p)
		{
			if (!ModelState.IsValid)
				return new ApiReturnType
				{
					Success = false,
					ErrorMessage = Constants.InvalidEmailAdress,
					ErrorCode = Constants.InvalidEmailAdressErrorCode
				};
			Microsoft.AspNet.Identity.PasswordHasher hash = new Microsoft.AspNet.Identity.PasswordHasher();
			var userPassword = Repository.Helpers.HelperMethods.GeneratePairCodeNum();
			var hashedPassword = hash.HashPassword(userPassword);
			return await repo.CreateUserLogin(p.Email,hashedPassword);
		}
		[HttpPost]
		[Route("User/CreateUserAccount")]
		public async Task<object> CreateUserAccount([FromBody]UserAccountDto p)
		{
			if (!ModelState.IsValid)//checks whether it is valid
				return new ApiReturnType
				{
					Success = false,
					ErrorMessage = Constants.InvalidEmailAdress,
					ErrorCode = Constants.InvalidEmailAdressErrorCode
				};
                return await repo.CreateUserAccount(p);
		}
		[HttpPost]
		[Route("User/UpdateUserProfile")]
		public async Task<object> UpdateUserProfile([FromBody] UserAccountDto p)
		{
			if (!ModelState.IsValid)//checks whether it is valid
			{
				return false;
			}
				return await repo.UpdateUserProfile(p);
			}
		}
    }
