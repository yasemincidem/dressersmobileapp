﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Repository.Dto;
using Repository.Implementations;
using Repository.Interface;
using Repository.Constants;

namespace DresserWebApi.Controllers
{
	public class ReservationController : ApiController
	{
		EfRepository repo = new EfRepository();
		[HttpPost]
		[Route("Reservation/CreateReservation")]
		public async Task<object> CreateReservation([FromBody]ReservationDto r)
		{
			if (!ModelState.IsValid)
			{
				new ApiReturnType()
				{
					Success = false,
					ErrorCode = Constants.ModalNotValidErrorCode,
					ErrorMessage = Constants.ModelNotFound
				};
			}
			return await repo.CreateReservation(r);
		}
		[HttpPost]
		[Route("Reservation/UpdateReservation")]
		public async Task<object> UpdateReservation([FromBody] ReservationDto r)
		{
			if (!ModelState.IsValid)
			{
				new ApiReturnType()
				{
					Success = false,
					ErrorCode = Constants.ModelNotFoundErrorCode,
					ErrorMessage = Constants.ModelNotFound
				};
			}
			return await repo.UpdateReservation(r);
		}
		[HttpPost]
		[Route("Reservation/GetReservationDetail")]
		public async Task<object> GetReservationDetail(string Id)
		{
			if (!ModelState.IsValid)
			{
				new ApiReturnType()
				{
					Success = false,
					ErrorCode = Constants.ModelNotFoundErrorCode,
					ErrorMessage = Constants.ModelNotFound
				};
           }
			return await repo.GetReservationDetail(Id);
		}
		[HttpPost]
		[Route("Reservation/DeleteReservation")]
		public async Task<object> DeleteReservation([FromBody]ReservationDto r)
		{
			if (!ModelState.IsValid)
			{
				new ApiReturnType()
				{
					Success = false,
					ErrorCode = Constants.ModelNotFoundErrorCode,
					ErrorMessage = Constants.ModelNotFound
				};
			}
			return await repo.DeleteReservation(r);
		}
    }
}

