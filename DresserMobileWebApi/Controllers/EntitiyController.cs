﻿using Repository.Constants;
using Repository.Dto;
using Repository.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace DresserWebApi.Controllers
{
    public class EntitiyController : ApiController
    {
		EfRepository repo = new EfRepository();
		[HttpPost]
		[Route("Entity/GetEntitiy")]
		public async Task<object> GetEntitiy([FromBody] GetEntityDto e)
		{
			if (!ModelState.IsValid)
			{
				new ApiReturnType()
				{
					Success = false,
					ErrorCode = Constants.ModelNotFoundErrorCode,
					ErrorMessage = Constants.ModelNotFound
				};
			}
			return await repo.GetEntitiy(e);
		}
    }
}
